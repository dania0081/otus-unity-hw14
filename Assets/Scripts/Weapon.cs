using System.Text;
using UnityEngine;

namespace Sample
{
    public sealed class Weapon : MonoBehaviour
    {
        [SerializeField]
        private float shootCountdown = 1.0f;

        [SerializeField]
        private Transform firePoint;
        
        [SerializeField]
        private GameObject bulletPrefab;
        
        private bool fireRequired;
        private Vector3 fireDirection;
        
        private float shootReloadTime;

        public void Shoot(Vector3 direction)
        {
            this.fireRequired = true;
            this.fireDirection = direction;
        }

        private void FixedUpdate()
        {
            if (!this.fireRequired)
            {
                return;
            }

            this.fireRequired = false;
            
            if (this.shootReloadTime > 0)
            {
                this.shootReloadTime -= Time.fixedDeltaTime;
                return;
            }

            var bullet = Instantiate(this.bulletPrefab, this.firePoint.position, Quaternion.LookRotation(this.fireDirection));
            bullet.GetComponent<Rigidbody>().velocity = this.fireDirection * 10.0f;
            this.shootReloadTime = this.shootCountdown;
        }
    }
}